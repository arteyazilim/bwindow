module.exports = function(grunt) {
    grunt.initConfig({
        uglify: {
            main: {
                files: {
                    'dist/bwindow.min.js': ['index.js']
                }
            }
        },
        cssmin: {
            main: {
                files: {
                    'dist/bwindow.min.css': ['bwindow.css']
                }
            }
        },
        yuidoc: {
            compile: {
                name: 'Bwindow',
                description: 'Bwindow Sanal Pencere',
                version: '1.0.0',
                options: {
                    paths: './',
                    outdir: 'doc/'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-yuidoc');

    grunt.registerTask('addlinefeed', 'Add Line Feed', function() {
        var fs = require('fs');
        fs.appendFileSync('dist/bwindow.min.js', "\r\n");
    });

    grunt.registerTask('default', 'Default Tasks', function() {
        grunt.task.run(['uglify', 'cssmin', 'addlinefeed', 'yuidoc']);
    });
};
