(function(root, factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define('Bwindow', ['qw', 'SStorage', 'EventDispatcher', 'Draggee'], factory);
    } else if (typeof module === 'object' && typeof module.exports === 'object') {
        module.exports = factory(require('qw'), require('SStorage'), require('EventDispatcher'), require('Draggee'));
    } else {
        root.Bwindow = factory(root.qw, root.SStorage, root.EventDispatcher, root.Draggee);
    }
})(this, function(qw, SStorage, EventDispatcher, Draggee) {
    'use strict';
    var document = window.document,
        defaultOptions = {
            // başlık metni
            title: null,

            // konum
            position: 'center',

            // modal mı?
            modal: true,

            // modal ise overlay'e tıklayınce pencere kapansın mı?
            overlayClose: false,

            // container'e eklenecek ek css sınıfı
            extraClass: null,

            // taşınabilir mi?
            draggable: true,

            // tam ekran olarak mı başlasın?
            maximized: false,

            // başlangıç boyut bilgisi
            size: {
                width: -1, // -1 auto anlamına gelir
                height: -1,
                // girilen width ve height contentSize mı containerSize mı?
                type: 'container'
            },

            // minimum boyut
            minSize: {
                width: 200,
                height: 16,
                type: 'content'
            },

            // maximum boyut
            maxSize: {
                width: '97%',
                height: '95%',
                type: 'container'
            },

            // butonlar
            buttons: {
                // üst butonlar
                toolbar: [
                    {
                        html: 'X',
                        cls: 'b-close',
                        fn: function(event, win) {
                            win.close();
                        }
                    }
                ],
                // alt sol butonlar
                left: [],
                // alt sağ butonlar
                right: []
            },

            // z-index değerine eklenecek sayıdır. normalde z-index otomatik olarak belirlenir.
            // buraya gireceğiniz değer otomatik olarak hesaplanan z-index değeriyle toplanıp
            // containerin z-indexi ayarlanacaktır.
            addExtraZIndex: 0,

            // scrollbarları kaldırmaya çalışır.
            // sadece addContent, setContent ile tetiklenir.
            autoRemoveScrollBars: false,

            // footer'a buton veya içerik eklendiğinde genişlik otomatik olarak artsın mı?
            // sadece addButton ve addFooterContent methodu ile tetiklenir.
            autoExpandByFooter: true,

            // header'a buton veya içerik eklendiğinde genişlik otomatik olarak artsın mı?
            // sadece addHeaderButton ve setTitle methodu ile tetiklenir.
            autoExpandByHeader: true,

            // boyutlandırma ve taşıma işlemi ardından eğer pencere dışarı taşmışsa düzeltir.
            autoFixPosition: true,

            // window resize olayından ne kadar süre sonra pencere yeniden konumlandırılsın/boyutlandırılsın?
            refreshTimeout: 300,

            // olaylar
            events: {}
        };

    var winResizeEvent = function(event)
    {
        var win = event.data.win;
        if (win._resizeTimeout !== false) {
            clearTimeout(win._resizeTimeout);
            win._resizeTimeout = false;
        }
        if (!('_els' in win)) {
            return true;
        }

        win._resizeTimeout = setTimeout(function()
        {
            win._updateSizeLimits();
            win.fixScroll();
            if (win._isMaximized) {
                win.maximize();
            } else {
                if (win.isDragged() === false) {
                    // aynı zamanda fixPosition'u da çalıştırır.
                    win.updatePosition();
                } else {
                    win.fixPosition();
                }
                //win.fixSize();
            }
        }, win._options.get('refreshTimeout'));
    };

    /**
     * Bwindow kurucusu.
     *
     * @class Bwindow
     * @constructor
     *
     * @param {String} content
     * @param {Object} options
     */
    function Bwindow(content, options)
    {
        if (!(this instanceof Bwindow)) {
            return new Bwindow(content, options);
        }

        // default ayarlar genişletiliyor.
        this._options = new SStorage(defaultOptions);
        if (window.defaultBwindowOptions && qw.isPlainObject(window.defaultBwindowOptions)) {
            this._options.extend(window.defaultBwindowOptions);
        }
        if (qw.isPlainObject(options)) {
            this._options.extend(options);
        }

        // pozisyon 'center' girilmişse iki elemanlı ve her
        // bir elemanı 'center' olan diziye çevriliyor.
        if (this._options.get('position') === 'center') {
            this._options.set('position', ['center', 'center']);
        }

        var posOpt = this._options.get('position');
        // pozisyon bilgisinin geçerliliği kontrol ediliyor.
        if (qw.isArray(posOpt) === false || posOpt.length !== 2) {
            throw new Error('Invalid value for position option.');
        }

        this._event = new EventDispatcher(this);
        var evtOpt = this._options.get('events');
        if (qw.isPlainObject(evtOpt)) {
            qw.each(evtOpt, function(fn, key) {
                this.on(key, fn);
            }, this);
        }

        this._ready = false;
        this._dragObj = null;
        this._isDragged = false;
        this._lastSize = null;
        this._lastPosition = null;
        this._lastDragStatus = null;
        this._isMaximized = false;
        this._resizeTimeout = false;

        this._createMainElements(content);

        // eğer başlık metni varsa.
        var titleOpt = this._options.get('title');
        if (qw.isString(titleOpt)) {
            this.setTitle(titleOpt);
        }

        // toolbar butonları ekleniyor.
        var btnOpt = this._options.get('buttons');
        if (btnOpt && qw.isArray(btnOpt.toolbar)) {
            qw.each(btnOpt.toolbar, function(item) {
                if (qw.isObject(item)) {
                    this.addHeaderButton(item.html, item.fn || null, item.cls || '', item.attrs || {});
                }
            }, this);
        }

        // sol butonlar ekleniyor.
        if (btnOpt && qw.isArray(btnOpt.left)) {
            qw.each(btnOpt.left, function(item) {
                if (qw.isObject(item)) {
                    if (!qw.hasOwn(item, 'button') || item.button) {
                        this.addButton('left', item.html || '', item.fn || null, item.cls || '', item.attrs || {}, item.focus || false);
                    } else {
                        this.addFooterContent('left', item.html || '');
                    }
                }
            }, this);
        }

        // sağ butonlar ekleniyor.
        if (btnOpt && qw.isArray(btnOpt.right)) {
            qw.each(btnOpt.right, function(item) {
                if (qw.isObject(item)) {
                    if (!qw.hasOwn(item, 'button') || item.button) {
                        this.addButton('right', item.html || '', item.fn || null, item.cls || '', item.attrs || {}, item.focus || false);
                    } else {
                        this.addFooterContent('right', item.html || '');
                    }
                }
            }, this);
        }

        // max-min değerler yazılıyor.
        this._updateSizeLimits();
        // opsiyonlardaki size değerlerine göre pencere boyutlandırılıyor.
        this._setInitialSize();

        // BU NOKTADA pencere, header, footer oluşturulmuş,
        // boyutlandırılmış ancak konumlandırılmamıştır.

        // ayarlarda autoExpandByHeader true ise
        // genişlik fixleniyor.
        if (this._options.get('autoExpandByHeader')) {
            this.expandByHeader();
        }

        // ayarlarda autoExpandByFooter true ise
        // genişlik fixleniyor.
        if (this._options.get('autoExpandByFooter')) {
            this.expandByFooter();
        }

        // transition ortalığı karıştırdığı için zorla kapatılıyor.
        this._els.container[0].style.transition = 'none';
        this._els.container[0].style.transitionProperty = 'none';

        // eğer içerikte scroll varsa kaldırılıyor.
        // scroll kaldırma yani height artırma işlemi
        // increaseHeight metodu ile yapıldığı için
        // maxSize değeri aşılmayacaktır.
        // var body = this._els.body[0],
        //     oldHeight = body.clientHeight,
        //     newHeight = 0;
        // if (body.scrollHeight > oldHeight) {
        //     body.style.position = 'static';
        //     body.style.overflowY = 'visible';
        //     newHeight = body.clientHeight;
        //     body.style.position = 'absolute';
        //     body.style.overflowY = 'auto';
        //     this.increaseHeight(newHeight - oldHeight);
        // }
        this.removeScrollBars(); // yukarıdaki commentlenen işlemleri yapar.

        // eğer opsiyonlarda modal belirtilmişse overlay gösteriliyor.
        if (this._options.get('modal')) {
            this.setModal();
        }

        // gösterilmeden önceki olay tetikleniyor
        this._event.trigger('before_show');

        // poziyon ilklendirmesi yapılıyor.
        // bu noktaya kadar pencere hiç bir zaman
        // ekranda görünmedi. bu noktada opsiyonlara
        // göre ekranda konumlandırılıyor (görünür
        // bir noktaya getiriliyor.)
        this.updatePosition();

        // eğer opsiyon olarak draggable true ise
        // pencere taşınabilir yapılıyor.
        if (this._options.get('draggable')) {
            this.setDraggable(true);
        }

        if (this._options.get('maximized')) {
            this.maximize();
        }

        // gösterildikten sonraki olay tetikleniyor
        this._event.trigger('after_show');

        qw(window).on('resize', {win: this}, winResizeEvent);

        // container mousedown olayında öne alınıyor.
        this._els.container.on('mousedown', {win: this}, function(event)
        {
            event.data.win.bringToFront();
        });
        this._ready = true;
    }

    Bwindow.lastZIndex = 2;

    /**
     * Container ve body elementlerini oluşturur.
     *
     * @method _createMainElements
     * @access private
     * @return {Bwindow}
     */
    Bwindow.prototype._createMainElements = function(content)
    {
        var exCls = this._options.get('extraClass');
        var exZIn = this._options.get('addExtraZIndex', 0);
        if (!qw.isNumber(exZIn)) {
            exZIn = 0;
        }
        // container oluşturuluyor.
        var container = qw.create('div')
            .addClass('bwindow' + (exCls && qw.isString(exCls) ? ' ' + exCls : ''))
            .css({
                'position': 'static',
                'display': 'inline-block',
                'margin': '0',
                'padding': '0',
                'z-index': exZIn + Bwindow.lastZIndex++
            });

        // body oluşturuluyor.
        var body = qw.create('div')
            .html(content)
            .addClass('b-body')
            .css({
                'position': 'static',
                'overflow': 'visible'
            })
            .appendTo(container);

        container
            .appendTo(document.body)
            .setTotalWidth(container[0].offsetWidth)
            .setTotalHeight(container[0].offsetHeight)
            .css({
                'position': 'fixed',
                'left': '-9999px',
                'top': 0
            });

        body.css({
            'overflow': 'auto',
            'position': 'absolute',
            'left': 0,
            'top': 0,
            'right': 0,
            'bottom': 0
        });

        // tüm elementleri tutan obje
        this._els = {
            container: container,
            body: body
        };
        return this;
    };

    Bwindow.prototype._updateSizeLimits = function()
    {
        var opt = this._options.toObject(),
            body = this._els.body,
            container = this._els.container,
            vpSize = qw.viewport(),
            maxWidth = parseWidth(opt.maxSize.width, vpSize.width),
            maxHeight = parseHeight(opt.maxSize.height, vpSize.height),
            minWidth = parseWidth(opt.minSize.width, vpSize.width),
            minHeight = parseHeight(opt.minSize.height, vpSize.height),
            hDiff = container.getTotalWidth() - body.getContentWidth(),
            vDiff = container.getTotalHeight() - body.getContentHeight();

        if (maxWidth === -1) {
            maxWidth = vpSize.width;
        }
        if (maxHeight === -1) {
            maxHeight = vpSize.height;
        }
        if (minWidth === -1) {
            minWidth = 0;
        }
        if (minHeight === -1) {
            minHeight = 0;
        }

        if (opt.maxSize.type === 'content') {
            maxWidth += hDiff;
            maxHeight += vDiff;
            if (maxWidth > vpSize.width) {
                maxWidth = vpSize.width;
            }
            if (maxHeight > vpSize.height) {
                maxHeight = vpSize.height;
            }
        }

        var cnt = container[0], ow, oh;
        if (this._ready) {
            ow = cnt.offsetWidth;
            oh = cnt.offsetHeight;
        }
        container.setTotalWidth(maxWidth, 'max-width');
        container.setTotalHeight(maxHeight, 'max-height');

        if (opt.minSize.type === 'content') {
            minWidth += hDiff;
            minHeight += vDiff;
        }
        container.setTotalWidth(minWidth, 'min-width');
        container.setTotalHeight(minHeight, 'min-height');
        if (this._ready) {
            if (
                ow != cnt.offsetWidth  ||
                oh != cnt.offsetHeight
            ) {
                this._event.trigger('after_resize');
            }
        }
    };

    /**
     * Başlangıç boyutunu ayarlara göre belirler.
     *
     * @method _setInitialSize
     * @access private
     */
    Bwindow.prototype._setInitialSize = function()
    {
        var vpSize = qw.viewport(),
            size = this._options.get('size'),
            type = size.type,
            width = parseWidth(size.width, vpSize.width),
            height = parseHeight(size.height, vpSize.height);

        // başlangıç genişliği ayarlanıyor.
        if (width > -1) {
            this[ type == 'content' ? 'setContentWidth' : 'setWidth' ](width);
        }

        // başlangıç genişliği ayarlanıyor.
        if (height > -1) {
            this[ type == 'content' ? 'setContentHeight' : 'setHeight' ](height);
        }
    };

    /**
     * Yeni olay kaydı yapar.
     *
     * @param  {String}   name Olay adı
     * @param  {Mixed}    data Olay verisi
     * @param  {Function} fn   Tetiklenecek method
     * @return {Bwindow}
     */
    Bwindow.prototype.on = function(name, data, fn)
    {
        if (arguments.length == 2) {
            fn = data;
            data = undefined;
        }
        this._event.on(name, data, fn);
        return this;
    };

    /**
     * Bir olay kaydını siler.
     *
     * @param  {String}   name Olay adı
     * @param  {Function} fn   Tetiklenecek method
     * @return {Bwindow}
     */
    Bwindow.prototype.off = function(name, fn)
    {
        this._event.off(name, fn);
        return this;
    };

    /**
     * Pencere genişliğini döndürür.
     *
     * @method getWidth
     * @access public
     * @return {Number}
     */
    Bwindow.prototype.getWidth = function()
    {
        return this._els.container.getTotalWidth();
    };

    /**
     * Pencere genişliğini ayarlar.
     *
     * @method setWidth
     * @access public
     * @param  {Number|String} width Number ya da string olarak yüzdelik değer.
     * @return {Bwindow}
     */
    Bwindow.prototype.setWidth = function(width)
    {
        // girilen değer parse ediliyor.
        width = parseWidth(width);

        // eğer değer 0'dan küçükse veya hatalıysa kabul edilmiyor.
        if (width === -1) {
            throw new Error('Invalid width value.');
        }

        this._event.trigger('before_resize', 'width', width);

        // kapsayıcının toplam genişliği ayarlanıyor.
        this._els.container.setTotalWidth(width);
        this._lastSize = this.getSize();

        // eğer hiç taşınmadıysa konum yenileniyor.
        if (!this.isDraggable() || !this.isDragged()) {
            // updatePosition aynı zamanda fixPosition'u da çalıştırır.
            this.updatePosition();
        } else if (this._options.get('autoFixPosition')) {
            this.fixPosition();
        }
        this._event.trigger('after_resize', 'width', width);

        // expandByFooter setWidth'i çağırdığı için sonsuz döngüye girer.
        /*if (this._options.get('autoExpandByFooter')) {
            this.expandByFooter();
        }*/
        return this;
    };

    /**
     * İçerik genişliğini ayarlar.
     *
     * @method setContentWidth
     * @access public
     * @param  {Number} width
     * @return {Bwindow}
     */
    Bwindow.prototype.setContentWidth = function(width)
    {
        var container = this._els.container,
            body = this._els.body;

        // girilen değer parse ediliyor.
        width = parseWidth(width);

        // eğer değer 0'dan küçükse veya hatalıysa kabul edilmiyor.
        if (width === -1) {
            throw new Error('Invalid width value.');
        }

        // body'nin border-box durumuna bakılmadan
        // horizontal padding, border ve margin
        // değerleri ekleniyor.
        width += qw.hPadding(body);
        width += qw.hBorder(body);
        width += qw.hMargin(body);

        if (container.isBorderBox() == false) {
            width += qw.hPadding(container);
            width += qw.hBorder(container);
        }
        this.setWidth(width);
        return this;
    };

    /**
     * Pencere yüksekliğini döndürür.
     *
     * @method getHeight
     * @access public
     * @return {Number}
     */
    Bwindow.prototype.getHeight = function()
    {
        return this._els.container.getTotalHeight();
    };

    /**
     * Pencere yükseliğini ayarlar.
     *
     * @method setHeight
     * @access public
     * @param  {Number|String} height Number ya da string olarak yüzdelik değer.
     * @return {Bwindow}
     */
    Bwindow.prototype.setHeight = function(height)
    {
        // girilen değer parse ediliyor.
        height = parseHeight(height);

        // eğer değer 0'dan küçükse veya hatalıysa kabul edilmiyor.
        if (height === -1) {
            throw new Error('Invalid height value.');
        }

        this._event.trigger('before_resize', 'height', height);

        // kapsayıcının toplam genişliği ayarlanıyor.
        this._els.container.setTotalHeight(height);
        this._lastSize = this.getSize();

        // eğer hiç taşınmadıysa konum yenileniyor.
        if (!this.isDraggable() || !this.isDragged()) {
            // updatePosition aynı zamanda fixPosition'u da çalıştırır.
            this.updatePosition();
        } else if (this._options.get('autoFixPosition')) {
            this.fixPosition();
        }
        this._event.trigger('after_resize', 'height', height);

        // expandByFooter setHeight'i çağırdığı için sonsuz döngüye girer.
        /*if (this._options.get('autoExpandByFooter')) {
            this.expandByFooter();
        }*/
        return this;
    };

    /**
     * İçerik yükseliğini ayarlar.
     *
     * @method setContentHeight
     * @access public
     * @param {Number} height
     */
    Bwindow.prototype.setContentHeight = function(height)
    {
        var container = this._els.container,
            body = this._els.body;

        // girilen değer parse ediliyor.
        height = parseHeight(height);

        // eğer değer 0'dan küçükse veya hatalıysa kabul edilmiyor.
        if (height === -1) {
            throw new Error('Invalid height value.');
        }

        // body'nin border-box durumuna bakılmadan
        // horizontal padding, border ve margin
        // değerleri ekleniyor.
        height += qw.vPadding(body);
        height += qw.vBorder(body);
        height += qw.vMargin(body);
        height += this.getHeaderHeight(true);
        height += this.getFooterHeight(true);

        if (container.isBorderBox() == false) {
            height += qw.vPadding(container);
            height += qw.vBorder(container);
        }

        this.setHeight(height);
        return this;
    };

    /**
     * Pencere boyutlarını döndürür.
     *
     * @method getSize
     * @access public
     * @return {Object} width ve height property'li obje.
     */
    Bwindow.prototype.getSize = function()
    {
        return {
            width: this.getWidth(),
            height: this.getHeight()
        };
    };

    /**
     * İçerik genişliğini döndürür.
     *
     * @method getContentWidth
     * @access public
     * @return {Number}
     */
    Bwindow.prototype.getContentWidth = function()
    {
        return this._els.body.getContentWidth();
    };

    /**
     * İçerik yüksekliğini döndürür.
     *
     * @method getContentHeight
     * @access public
     * @return {Number}
     */
    Bwindow.prototype.getContentHeight = function()
    {
        return this._els.body.getContentHeight();
    };

    /**
     * İçerik boyutlarını döndürür.
     *
     * @method getContentSize
     * @access public
     * @return {Object} width ve height property'li obje.
     */
    Bwindow.prototype.getContentSize = function()
    {
        return {
            width: this.getContentWidth(),
            height: this.getContentHeight()
        };
    };

    /**
     * Genişliği artırır.
     *
     * @method increaseWidth
     * @access public
     * @param  {Number|String} width Sayısal değer ya da yüzdelik değer.
     * @return {Bwindow}
     */
    Bwindow.prototype.increaseWidth = function(width)
    {
        this.setWidth(this.getWidth() + parseWidth(width));
        return this;
    };

    /**
     * Yükseliği artırır.
     *
     * @method increaseHeight
     * @access public
     * @param  {Number|String} height Sayısal değer ya da yüzdelik değer.
     * @return {Bwindow}
     */
    Bwindow.prototype.increaseHeight = function(height)
    {
        this.setHeight(this.getHeight() + parseHeight(height));
        return this;
    };

    /**
     * Genişiği azaltır.
     *
     * @method reduceWidth
     * @access public
     * @param  {Number|String} width Sayısal değer ya da yüzdelik değer.
     * @return {Bwindow}
     */
    Bwindow.prototype.reduceWidth = function(width)
    {
        this.setWidth(this.getWidth() - parseWidth(width));
        return this;
    };

    /**
     * Yükseliği azaltır.
     *
     * @method reduceHeight
     * @access public
     * @param  {Number|String} height Sayısal değer ya da yüzdelik değer.
     * @return {Bwindow}
     */
    Bwindow.prototype.reduceHeight = function(height)
    {
        this.setHeight(this.getHeight() - parseHeight(height));
        return this;
    };

    /**
     * Pencere boyutunu belirler.
     *
     * Bwindow.setWidth(...).setHeight(...) yapar.
     *
     * @method resizeTo
     * @access public
     * @param  {Number|String} width  Sayısal değer ya da string olarak yüzde.
     * @param  {Number|String} height Sayısal değer ya da string olarak yüzde.
     * @param  {String}        type   content ya da varsayılan olarak container
     * @return {Bwindow}
     */
    Bwindow.prototype.resizeTo = function(width, height, type)
    {
        if (type === 'content') {
            this.setContentWidth(width).setContentHeight(height);
        } else {
            this.setWidth(width).setHeight(height);
        }
        return this;
    };

    /**
     * Dikey scrollbarları kaldırır.
     *
     * @method removeVScrollBars
     * @access public
     * @param  {Function} fn İşlemden sonra çalıştırılır.
     * @return {Bwindow}
     */
    Bwindow.prototype.removeScrollBars = function(fn)
    {
        var container = this._els.container[0],
            body = this._els.body[0],
            width = container.style.width,
            height = container.style.height,
            newWidth, newHeight, prevWidth, prevHeight;

        prevWidth = this._els.body.getContentWidth();
        prevHeight = this._els.body.getContentHeight();

        container.style.width = '';
        container.style.height = '';
        body.style.position = 'static';

        newWidth = this._els.body.getContentWidth();
        newHeight = this._els.body.getContentHeight();

        container.style.width = width;
        container.style.height = height;
        body.style.position = 'absolute';

        if (newWidth > prevWidth) {
            this.setContentWidth(newWidth);
        }
        if (newHeight > prevHeight) {
            this.setContentHeight(newHeight);
        }

        //this.fixScroll();
        return this;
    };

    /**
     * Scrollbarlar gereksiz ise kaldırır.
     *
     * @method fixScroll
     * @access public
     * @param  {Function} fn Fixleme işleminden sonra çalıştırır.
     * @return {Bwindow}
     */
    Bwindow.prototype.fixScroll = function(fn)
    {
        var that = this, body = this._els.body[0];
        if (!qw.isFunction(fn)) {
            fn = false;
        }
        body.style.overflowX = 'hidden';
        body.style.overflowY = 'hidden';
        setTimeout(function() {
            body.style.overflowX = 'auto';
            body.style.overflowY = 'auto';
            if (fn) {
                fn(that);
            }
        }, 50);
        return this;
    };

    /**
     * Pencerenin maximum boyutunu belirler.
     *
     * @method setMaxSize
     * @access public
     * @param  {Number|String} width  Sayısal değer ya da string olarak yüzde.
     * @param  {Number|String} height Sayısal değer ya da string olarak yüzde.
     * @param  {String}        type   content ya da varsayılan olarak window
     * @return {Bwindow}
     */
    Bwindow.prototype.setMaxSize = function(width, height, type)
    {
        this._options.set('maxSize', {
            width: width,
            height: height,
            type: type !== 'content' ? 'container' : 'content'
        });
        this._updateSizeLimits();
        return this;
    };

    /**
     * Pencerenin minimum boyutunu belirler.
     *
     * @method setMinSize
     * @access public
     * @param  {Number|String} width  Sayısal değer ya da string olarak yüzde.
     * @param  {Number|String} height Sayısal değer ya da string olarak yüzde.
     * @param  {String}        type   content ya da varsayılan olarak window
     * @return {Bwindow}
     */
    Bwindow.prototype.setMinSize = function(width, height, type)
    {
        this._options.set('minSize', {
            width: width,
            height: height,
            type: type !== 'content' ? 'container' : 'content'
        });
        this._updateSizeLimits();
        return this;
    };

    /**
     * Başlığın yüksekliğini döndürür.
     *
     * @method getHeaderHeight
     * @access public
     * @param  {Boolean} withMargin Margin dahil edilsin mi?
     * @return {Number}
     */
    Bwindow.prototype.getHeaderHeight = function (withMargin)
    {
        if (this.hasHeader() === false) {
            return 0;
        }
        return this._els.header.getTotalHeight(withMargin);
    };

    /**
     * Footer yüksekliğini döndürür.
     *
     * @method getFooterHeight
     * @access public
     * @param  {Boolean} withMargin Marginler dahil edilsin mi?
     * @return {Number}
     */
    Bwindow.prototype.getFooterHeight = function (withMargin)
    {
        if (this.hasFooter() === false) {
            return 0;
        }
        return this._els.footer.getTotalHeight(withMargin);
    };

    /**
     * Pencere konumunu değiştirir.
     *
     * @method moveTo
     * @access public
     * @param  {Number}  left
     * @param  {Number}  top
     * @param  {Boolean} animate
     * @return {Bwindow}
     */
    Bwindow.prototype.moveTo = function(left, top, animate)
    {
        var size = this.getSize(),
            vpSize = qw.viewport(),
            leftSuffix = 'px',
            rightSuffix = 'px',
            changeDrag = true;

        if (left === 'center') {
            left = Math.round((vpSize.width - size.width) / 2);
        } else if (!qw.isNumber(left)) {
            leftSuffix = '';
            changeDrag = false;
        }

        if (top === 'center') {
            top = Math.round((vpSize.height - size.height) / 2);
        } else if (!qw.isNumber(top)) {
            rightSuffix = '';
            changeDrag = false;
        }

        this._event.trigger('before_move', left, top);
        this._els.container.css({
            left: left + leftSuffix,
            top: top + rightSuffix
        });
        this._event.trigger('after_move', left, top);

        if (this._options.get('autoFixPosition')) {
            this.fixPosition();
        }
        if (changeDrag) {
            this._isDragged = false;
        }
        return this;
    };

    /**
     * Pencereyi ekranın yatay ve dikey olarak ortasına taşır.
     *
     * @method moveToCenter
     * @access public
     * @return {Bwindow}
     */
    Bwindow.prototype.moveToCenter = function()
    {
        this.moveTo('center', 'center');
        return this;
    };

    /**
     * Pencereyi ayarlara göre yeniden konumlandırır.
     *
     * @method updatePosition
     * @access public
     * @return {Bwindow}
     */
    Bwindow.prototype.updatePosition = function()
    {
        var pos = this._options.get('position');
        this.moveTo(pos[ 0 ], pos[ 1 ]);
        // autoFixPosition true ise moveTo zaten fixPosition
        // yapacağı için ikinci yapılması engelleniyor.
        if (!this._options.get('autoFixPosition')) {
            this.fixPosition();
        }
        return this;
    };

    /**
     * Pencerenin konumunu döndürür.
     *
     * @method getPosition
     * @access public
     * @return {Object}
     */
    Bwindow.prototype.getPosition = function(animate)
    {
        return {
            left: this._els.container[0].offsetLeft,
            top: this._els.container[0].offsetTop
        };
    };

    /**
     * Pencerenin boyutunu eğer dışarı taşmışsa düzeltir.
     *
     * @method fixSize
     * @access public
     * @return {Bwindow}
     */
    Bwindow.prototype.fixSize = function(animate)
    {
        var vpSize = qw.viewport(),
            size = this.getSize();

        if (size.width > vpSize.width) {
            this.setWidth(vpSize.width);
        }
        if (size.height > vpSize.height) {
            this.setHeight(vpSize.height);
        }
        return this;
    };

    /**
     * Pencerenin konumunu eğer dışarı taşmışsa düzeltir.
     *
     * @method fixPosition
     * @access public
     * @return {Bwindow}
     */
    Bwindow.prototype.fixPosition = function(animate)
    {
        var vpSize = qw.viewport(),
            container = this._els.container[0];

        if (container.offsetLeft + container.offsetWidth > vpSize.width) {
            container.style.left = (vpSize.width - container.offsetWidth) + 'px';
        }
        if (container.offsetLeft < 0) {
            container.style.left = '0';
        }

        if (container.offsetTop + container.offsetHeight > vpSize.height) {
            container.style.top = (vpSize.height - container.offsetHeight) + 'px';
        }
        if (container.offsetTop < 0) {
            container.style.top = '0';
        }
        return this;
    };

    /**
     * Pencereyi maximize eder.
     *
     * Max değerler aşılmaz.
     *
     * @method maximize
     * @access public
     * @return {Bwindow}
     */
    Bwindow.prototype.maximize = function()
    {
        var vpSize = qw.viewport(),
            maxSize = this._options.get('maxSize'),
            maxWidth = parseWidth(maxSize.width, vpSize.width),
            maxHeight = parseHeight(maxSize.height, vpSize.height);

        if (maxWidth < 0) {
            maxWidth = vpSize.width;
        }
        if (maxHeight < 0) {
            maxHeight = vpSize.height;
        }

        if (!this._isMaximized) {
            this._lastSize = this.getSize();
            this._lastPosition = this.getPosition();
            this._lastDragStatus = this.isDraggable();
        }
        this._isMaximized = true;

        this.resizeTo(maxWidth, maxHeight, maxSize.type);
        this.moveTo('center', 'center');
        this.setDraggable(false);
        return this;
    };

    /**
     * Pencere maximize edilmiş mi?
     *
     * @method isMaximized
     * @access public
     * @return {Boolean}
     */
    Bwindow.prototype.isMaximized = function()
    {
        return !!this._isMaximized;
    };

    /**
     * Pencere maximize edildiyse eski boyutlarına döndürür.
     *
     * @method restore
     * @access public
     * @return {Bwindow}
     */
    Bwindow.prototype.restore = function()
    {
        if (!this._lastSize || !this._lastPosition) {
            return this;
        }
        this.resizeTo(this._lastSize.width, this._lastSize.height);
        this.moveTo(this._lastPosition.left, this._lastPosition.top);
        this.setDraggable(this._lastDragStatus);
        this._lastSize = null;
        this._lastPosition = null;
        this._lastDragStatus = null;
        this._isMaximized = false;
        return this;
    };

    /**
     * Bwindow elementlerini döndürür.
     *
     * @method getElements
     * @access public
     * @return {Object}
     */
    Bwindow.prototype.getElements = function()
    {
        return this._els;
    };

    /**
     * Container elementini döndürür.
     *
     * @method getContainer
     * @access public
     * @param  {Boolean} nativeElement
     * @return {Element}
     */
    Bwindow.prototype.getContainer = function(nativeElement)
    {
        if (nativeElement) {
            return this._els.container[0];
        }
        return this._els.container;
    };

    /**
     * Body elementini döndürür.
     *
     * @method getBody
     * @access public
     * @param  {Boolean} nativeElement
     * @return {Element}
     */
    Bwindow.prototype.getBody = function(nativeElement)
    {
        if (nativeElement) {
            return this._els.body[0];
        }
        return this._els.body;
    };

    /**
     * Ayarları döndürür.
     *
     * @method getOptions
     * @access public
     * @return {Object} Plain object
     */
    Bwindow.prototype.getOptions = function()
    {
        return this._options.toObject();
    };

    /**
     * İçeriği temizler.
     *
     * @method empty
     * @access public
     * @return {Bwindow}
     */
    Bwindow.prototype.empty = function()
    {
        this._els.body.empty();
        if (this._options.get('autoFixPosition')) {
            this.fixPosition();
        }
        return this;
    };

    /**
     * İçeriği belirler/değiştirir.
     *
     * @method setContent
     * @access public
     * @param  {String|Element} html
     * @return {Bwindow}
     */
    Bwindow.prototype.setContent = function(html)
    {
        if (!html) {
            return this.empty();
        }

        this._els.body.html(html);
        if (this._options.get('autoRemoveScrollBars')) {
            this.removeScrollBars();
        }
        if (this._options.get('autoFixPosition')) {
            this.fixPosition();
        }
        return this;
    };

    /**
     * İçeriğe ekleme yapar.
     *
     * @method addContent
     * @access public
     * @param  {String|Element} html
     * @return {Bwindow}
     */
    Bwindow.prototype.addContent = function(html)
    {
        this._els.body.append(html);
        if (this._options.get('autoRemoveScrollBars')) {
            this.removeScrollBars();
        }
        if (this._options.get('autoFixPosition')) {
            this.fixPosition();
        }
        return this;
    };

    /**
     * İçeriği döndürür.
     *
     * @method getContent
     * @access public
     * @return {String}
     */
    Bwindow.prototype.getContent = function()
    {
        return this._els.body.html();
    };

    /**
     * Pencerede başlık var mı?
     *
     * @method hasHeader
     * @access public
     * @return {Boolean}
     */
    Bwindow.prototype.hasHeader = function()
    {
        return qw.hasOwn(this._els, 'header');
    };

    /**
     * Pencere başlığını oluşturur.
     *
     * @method _createHeader
     * @access private
     * @return {Bwindow}
     */
    Bwindow.prototype._createHeader = function()
    {
        if (this.hasHeader()) {
            return this;
        }

        this._els.header = qw.create('div')
            .addClass('b-header')
            .css({
                position: 'absolute',
                left: '0',
                top: '0',
                right: '0',
                bottom: 'auto'
            })
            .prependTo(this._els.container);

        this._els.title = qw.create('h3')
            .addClass('b-title')
            .html('<span></span>')
            .appendTo(this._els.header);
        this._els.toolbar = qw.create('div')
            .addClass('b-toolbar inp-grp')
            .appendTo(this._els.header);

        var height = this.getHeaderHeight(true);
        this.increaseHeight(height);
        this._els.body[0].style.top = height + 'px';
        return this;
    };

    /**
     * Header içeriğine göre pencere genişliğini artırır.
     *
     * @method expandByHeader
     * @access public
     * @return {Bwindow}
     */
    Bwindow.prototype.expandByHeader = function()
    {
        if (this.hasHeader() === false) {
            return this;
        }
        var e = this._els, hd = e.header.getContentWidth(),
            hi = e.title.getTotalWidth(true) - e.title.getContentWidth(),
            iw = e.title.getChildren().getTotalWidth(true) + e.toolbar.getTotalWidth(true);

        if (hd < iw + hi) {
            this.increaseWidth(iw + hi - hd);
        }
        return this;
    };

    /**
     * Pencere başlığı elementini siler.
     *
     * @method removeHeader
     * @access public
     * @param  {Boolean} reduceHeight
     * @return {Bwindow}
     */
    Bwindow.prototype.removeHeader = function(reduceHeight)
    {
        if (this.hasHeader()) {
            this.setDraggable(false);
            if (reduceHeight) {
                this.reduceHeight(this.getHeaderHeight(true));
            }
            this._els.header.remove();
            delete this._els.header;
            delete this._els.title;
            delete this._els.toolbar;
            this._els.body[0].style.top = '0px';
        }
        return this;
    };

    /**
     * Başlık metnini döndürür.
     *
     * @method getTitle
     * @access public
     * @return {String}
     */
    Bwindow.prototype.getTitle = function()
    {
        if (this.hasHeader()) {
            return this._els.title[0].firstChild.innerHTML;
        }
        return undefined;
    };

    /**
     * Başlık metnini belirler/değiştirir.
     *
     * @method setTitle
     * @access public
     * @param  {String}  html
     * @return {Bwindow}
     */
    Bwindow.prototype.setTitle = function(html)
    {
        this._createHeader();
        this._els.title[0].firstChild.innerHTML = html;
        if (this._options.get('autoExpandByHeader')) {
            this.expandByHeader();
        }
        return this;
    };

    /**
     * Başlığa buton ekler.
     *
     * @todo seçenekler tek parametrede toplanacak.
     *
     * @method addHeaderButton
     * @access private
     * @param  {String}   html
     * @param  {Function} onclick
     * @param  {String}   cls
     * @param  {Object}   attrs
     * @return {Bwindow}
     */
    Bwindow.prototype.addHeaderButton = function(html, onclick, cls, attrs)
    {
        this._createHeader();
        var btn = qw.create('button')
            .addClass(cls || 'btn btn-sm')
            .html(html)
            .attr('type', 'button')
            .appendTo(this._els.toolbar);

        if (qw.isObject(attrs)) {
            btn.attr(attrs);
        }
        if (qw.isFunction(onclick)) {
            btn.on('click', {win: this}, function(event)
            {
                onclick(event, event.data.win);
            });
        }
        if (this._options.get('autoExpandByHeader')) {
            this.expandByHeader();
        }
        return this;
    };

    /**
     * Pencere footer'ı var mı?
     *
     * @method hasFooter
     * @access public
     * @return {Boolean}
     */
    Bwindow.prototype.hasFooter = function()
    {
        return qw.hasOwn(this._els, 'footer');
    };

    /**
     * Footer oluşturur.
     *
     * @method _createFooter
     * @access private
     * @return {Bwindow}
     */
    Bwindow.prototype._createFooter = function()
    {
        if (this.hasFooter()) {
            return this;
        }
        this._els.footer = qw.create('div')
            .addClass('b-footer')
            .appendTo(this._els.container)
            .css({
                position: 'absolute',
                left: '0',
                top: 'auto',
                right: '0',
                bottom: '0'
            });
        this._els.footerLeft = qw.create('div')
            .addClass('b-footer-left')
            .appendTo(this._els.footer);
        this._els.footerRight = qw.create('div')
            .addClass('b-footer-right')
            .appendTo(this._els.footer);

        var height = this.getFooterHeight(true);
        this.increaseHeight(height);
        this._els.body[0].style.bottom = height + 'px';
        return this;
    };

    /**
     * Footer içeriğine göre pencere genişliğini artırır.
     *
     * @method expandByFooter
     * @access public
     * @return {Bwindow}
     */
    Bwindow.prototype.expandByFooter = function()
    {
        if (this.hasFooter() === false) {
            return this;
        }
        var foots = this._els.footer.getChildren(),
            total = foots.get(0).getTotalWidth(true) + foots.get(1).getTotalWidth(true),
            curr = this._els.footer.getContentWidth();

        if (curr < total) {
            this.increaseWidth(total - curr);
        }
        return this;
    };

    /**
     * Footer elementini siler.
     *
     * @method removeFooter
     * @access public
     * @param  {Boolean} reduceHeight
     * @return {Bwindow}
     */
    Bwindow.prototype.removeFooter = function(reduceHeight)
    {
        if (this.hasFooter()) {
            if (reduceHeight) {
                this.reduceHeight(this.getFooterHeight(true));
            }
            this._els.footer.remove();
            delete this._els.footer;
            delete this._els.footerLeft;
            delete this._els.footerRight;
            this._els.body[0].style.bottom = '0px';
        }
        return this;
    };

    /**
     * Footer'a buton ekler.
     *
     * @todo seçenekler tek parametrede toplanacak.
     *
     * @method addButton
     * @access public
     * @param  {String}   align   left ya da right girilmelidir.
     * @param  {String}   html
     * @param  {Function} onclick
     * @param  {String}   cls
     * @param  {Object}   attrs
     * @param  {Boolean}  focus
     * @return {Bwindow}
     */
    Bwindow.prototype.addButton = function(align, html, onclick, cls, attrs, focus)
    {
        this._createFooter();
        var btn = qw.create('button')
            .addClass(cls || 'btn btn-sm')
            .html(html)
            .attr('type', 'button');
        if (qw.isObject(attrs)) {
            btn.attr(attrs);
        }
        if (qw.isFunction(onclick)) {
            btn.on('click', {win: this}, function(event)
            {
                onclick(event, event.data.win);
            });
        }
        if (qw.isString(align) && align.toLowerCase() === 'left') {
            this._els.footerLeft.append(btn);
        } else {
            this._els.footerRight.append(btn);
        }
        if (focus) {
            btn.focus();
        }
        if (this._options.get('autoExpandByFooter')) {
            this.expandByFooter();
        }
        return this;
    };

    /**
     * Footer'a içerik ekler.
     *
     * @method addFooterContent
     * @access public
     * @param  {String}          align   left ya da right
     * @param  {String|Element}  content
     * @return {Bwindow}
     */
    Bwindow.prototype.addFooterContent = function(align, content)
    {
        var footerElement;
        this._createFooter();
        if (qw.isString(align) && align.toLowerCase() === 'left') {
            footerElement = this._els.footerLeft;
        } else {
            footerElement = this._els.footerRight;
        }
        footerElement.append(content);
        if (this._options.get('autoExpandByFooter')) {
            this.expandByFooter();
        }
        return this;
    };

    /**
     * Pencereyi kapatılmış mı?
     *
     * @method isClosed
     * @access public
     * @return {Boolean}
     */
    Bwindow.prototype.isClosed = function()
    {
        return !('_els' in this && this._els);
    };

    /**
     * Pencereyi kapatır.
     *
     * @method close
     * @access public
     * @return {void}
     */
    Bwindow.prototype.close = function()
    {
        var result = false;
        // listenin sonuna yeni bir olay ekleniyor.
        // eğer bu eklenenden önceki olaylardan biri
        // event.preventDefault() çalıştırısa result
        // değişkeninin değeri false olarak kalacağından
        // kapanma işlemi gerçekleştirilmeyecektir.
        // yani: kapanma işlemini iptal etmek için
        // before_close'a kaydedilen olaylardan birinin
        // event.preventDefault() çalıştırması gerekir.
        this.on('before_close', function() {
            result = true;
        });
        this._event.trigger('before_close');

        // kapatma iptal ediliyor.
        if (result === false) {
            return;
        }
        if (this.isModal()) {
            this._els.overlay.remove();
        }
        this._els.container.remove();
        this._event.trigger('after_close');
        qw(window).off('resize', winResizeEvent);
        if (this._resizeTimeout !== false) {
            clearTimeout(this._resizeTimeout);
        }
        delete this._els;
        delete this._dragObj;
        delete this._event;
        delete this._options;
        delete this._lastSize;
        delete this._lastPosition;
        /*delete this._isDragged;
        delete this._isMaximized;
        delete this._lastDragStatus;
        delete this._ready;
        delete this._resizeTimeout;*/
        return undefined;
    };

    /**
     * Pencere modal durumda mı?
     *
     * @method isModal
     * @access public
     * @return {Boolean}
     */
    Bwindow.prototype.isModal = function()
    {
        return 'overlay' in this._els;
    };

    /**
     * Pencere arkasında perde gösterir.
     *
     * @method setModal
     * @access public
     * @param  {Boolean} modal
     * @return {Bwindow}
     */
    Bwindow.prototype.setModal = function(modal)
    {
        var modal = arguments.length === 0 ? true : !!modal;
        if (modal && !this.isModal()) {
            var ex = this._options.get('extraClass'),
                cls = 'bwindow-overlay' + (ex && qw.isString(ex) ? ' ' + ex : ''),
                exZIn = this._options.get('addExtraZIndex', 0);
            if (!qw.isNumber(exZIn)) {
                exZIn = 0;
            }
            this._els.overlay = qw.create('div').addClass(cls).appendTo(document.body);
            this._els.overlay[0].style.zIndex = this._els.container[0].style.zIndex;
            this._els.container[0].style.zIndex = exZIn + Bwindow.lastZIndex++;
            if (this._options.get('overlayClose')) {
                this._els.overlay.on('click', {win: this}, function(event) {
                    event.data.win.close();
                });
            }
        } else if (!modal && this.isModal()) {
            this._els.overlay.remove();
            delete this._els.overlay;
        }
        return this;
    };

    /**
     * Pencereyi diğer pencerelerin z yönünde önüne taşır.
     *
     * @method bringToFront
     * @access public
     * @return {Bwindow}
     */
    Bwindow.prototype.bringToFront = function()
    {
        var exZIn = this._options.get('addExtraZIndex', 0);
        if (!qw.isNumber(exZIn)) {
            exZIn = 0;
        }
        if (Bwindow.lastZIndex - 1 === +this._els.container[0].style.zIndex) {
            return this;
        }
        if (this.isModal()) {
            this._els.overlay[0].style.zIndex = exZIn + Bwindow.lastZIndex++;
        }
        this._els.container[0].style.zIndex = exZIn + Bwindow.lastZIndex++;
        return this;
    };

    /**
     * Pencere taşınabilir olarak ayarlanmış mı?
     *
     * @method isDraggable
     * @access public
     * @return {Boolean}
     */
    Bwindow.prototype.isDraggable = function()
    {
        return Draggee && this._dragObj && this._dragObj instanceof Draggee;
    };

    /**
     * Pencere hiç taşınmış mı?
     *
     * @method isDragged
     * @access public
     * @return {Boolean}
     */
    Bwindow.prototype.isDragged = function()
    {
        return !!this._isDragged;
    };

    /**
     * Pencerenin Draggee objesini döndürür.
     *
     * @method getDragObject
     * @access public
     * @return {Draggee}
     */
    Bwindow.prototype.getDragObject = function()
    {
        return this._dragObj;
    };

    /**
     * Pencerenin taşınabilirlik durumunu belirler.
     *
     * @method setDraggable
     * @access public
     * @param  {Boolean} draggable
     * @return {Bwindow}
     */
    Bwindow.prototype.setDraggable = function(draggable)
    {
        if (!Draggee) {
            return this;
        }
        var that = this, setDrag = arguments.length === 0 ? true : !!draggable;
        if (setDrag && !this.isDraggable()) {
            if (this.hasHeader() === false) {
                return this;
            }
            var opts = {
                zIndex: false,
                handler: this._els.title[0],
                onStart: function()
                {
                    that._isDragged = true;
                }
            };
            this._dragObj = new Draggee(this._els.container[0], opts);
            this.addClass('b-draggable');
        } else if(!setDrag && this.isDraggable()) {
            this._dragObj.destroy();
            this._dragObj = null;
            this.removeClass('b-draggable');
        }
        return this;
    };

    /**
     * Pencereye css sınıfı ekler.
     *
     * @method addClass
     * @access public
     * @param  {String} cls Bir veya daha fazla sınıf adı.
     * @return {Bwindow}
     */
    Bwindow.prototype.addClass = function(cls)
    {
        this._els.container.addClass(cls);
        return this;
    };

    /**
     * Pencere sınıflarında varlık kontrolü yapar.
     *
     * @method hasClass
     * @access public
     * @param  {String}  cls Bir veya daha fazla sınıf adı.
     * @return {Boolean}
     */
    Bwindow.prototype.hasClass = function(cls)
    {
        return this._els.container.hasClass(cls);
    };

    /**
     * Pencereden bir veya daha fazla css sınıfı siler.
     *
     * @method removeClass
     * @access public
     * @param  {String} cls Bir veya daha fazla sınıf adı.
     * @return {Bwindow}
     */
    Bwindow.prototype.removeClass = function(cls)
    {
        this._els.container.removeClass(cls);
        return this;
    };

    function parseWidth(width, max)
    {
        if (!qw.isNumber(max)) {
            max = qw.viewport().width;
        }
        if (qw.isString(width)) {
            if (width.slice(-1) === '%') {
                width = parseFloat(width.slice(0, -1));
                width = max * width / 100;
            } else if(width.slice(-2).toLowerCase() === 'px') {
                width = parseFloat(width.slice(0, -2));
            }
        }
        if (!qw.isNumber(width) || width < 0) {
            return -1;
        }
        return Math.round(width);
    }

    // parseHeight(height, qw.viewport().height) olarak yeniler
    function parseHeight(height, max)
    {
        if (!qw.isNumber(max)) {
            max = qw.viewport().height;
        }
        if (qw.isString(height)) {
            if (height.slice(-1) === '%') {
                height = parseFloat(height.slice(0, -1));
                height = max * height / 100;
            } else if(height.slice(-2).toLowerCase() === 'px') {
                height = parseFloat(height.slice(0, -2));
            }
        }
        if (!qw.isNumber(height) || height < 0) {
            return -1;
        }
        return Math.round(height);
    }
    return Bwindow;
});
